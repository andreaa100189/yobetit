
import React, { Component } from "react";

class CapitalForm extends Component {
    constructor(props) {
        super(props);
        this.state = { capital: "" };
    }

  

    changeHandler = (e) =>{
        this.setState({[e.target.name]: e.target.value})
    }

    //get the data according to client capital
    submitHandler = e => {
        e.preventDefault()
         fetch("http://localhost:9000/backend/"+ this.state['capital'])
        .then(res => res.json())
        .then(data => this.initialize(data))
        .catch(err => console.log("Error:", err));

    }

    //filling up the data 
    initialize (countriesData) {
        document.getElementById("capital").innerHTML = countriesData[0].name;
        document.querySelector("#flag-container img").src =  countriesData[0].flag;
        document.querySelector("#flag-container img").alt = `Flag of ${ countriesData[0].name}`;  
        document.getElementById("dialing-code").innerHTML = `+${ countriesData[0].callingCodes[0]}`;
        document.getElementById("population").innerHTML =  countriesData[0].population.toLocaleString("en-US");
        document.getElementById("currencies").innerHTML =  countriesData[0].currencies.filter(c => c.name).map(c => `${c.name} (${c.code})`).join(", ");
        document.getElementById("region").innerHTML =  countriesData[0].region;
        document.getElementById("subregion").innerHTML =  countriesData[0].subregion;
        
        }

    render() {
        const {capital} = this.state
        return (
           <div className="background-white">
            <form onSubmit={this.submitHandler}>
                <div>
                    <input placeholder="Type here.." className="search-input" type="text" name="capital" value={capital} onChange={this.changeHandler}/>
                </div>
                <button className="button-submit" type="submit">Submit</button>               
            </form>
            <div id="country-details">
                 <div id="flag-container">
                    <img src="" alt=""/>
                 </div>
                <p>Capital: <span id="capital"></span></p>
                <p>Dialing Code: <span id="dialing-code"></span></p>
                <p>Population: <span id="population"></span></p>
                <p>Currencies: <span id="currencies"></span></p>
                <p>Region: <span id="region"></span></p>
                <p>Subregion: <span id="subregion"></span></p>
            </div>
          
            </div>

           
        );
    }
}

export default CapitalForm;
