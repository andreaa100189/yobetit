
import React, { Component } from "react";

class Test2 extends Component {
    constructor(props) {
        super(props);
    }

  

    changeHandler = (e) =>{
        this.setState({[e.target.name]: e.target.value})
    }

    //get countries that include part of the strings
    submitHandler = e => {
        fetch("http://localhost:9000/test2/")
        .then(res => res.json())
        .then(data => this.initialize(data))
        .catch(err => console.log("Error:", err));
      }

    //dispay data in the table
    initialize (countriesData) {
        var table = document.createElement("table");

        var tr ;                   

        for (var i = 0; i < countriesData.length; i++) {

            tr = table.insertRow(-1);
                tr.innerHTML = countriesData[i];
        }

        var divContainer = document.getElementById("showData");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);
        }
       
    render() {
         return (
           <div className="background-white">
               {this.submitHandler()}
               <p className="DisplayData" id='showData'></p>
           </div>
        );
    }
}

export default Test2;
