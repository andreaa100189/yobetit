
import React, { Component } from "react";

class MachineSlot extends Component {
    
    constructor(props) {
        super(props);
    }
    
    changeHandler = (e) =>{
        this.setState({[e.target.name]: e.target.value})
    }

    initialize (countriesData) {
        countriesData.preventDefault()
        // getting value of credits left and converting it to int
        var credits = parseInt(document.getElementById('credits').innerHTML);
        // reducing credits by one
        --credits;
        //checking if there are credits left        
        if(credits === -1){
        document.getElementById("message").innerHTML = "NO CREDIT LEFT!";
            return;}

        // if credits is bigger than 0 display credits left    
        document.getElementById('credits').innerHTML = credits;
         // getting value of score and converting it to int
        var score = parseInt(document.getElementById('score').innerHTML);
        var Reel1 = ["cherry", "lemon", "apple", "lemon", "banana", "banana", "lemon", "lemon"];
        var Reel2 = ["lemon", "apple", "lemon", "lemon", "cherry", "apple", "banana", "lemon"];
        var Reel3 = ["lemon", "apple", "lemon", "apple", "cherry", "lemon", "banana", "lemon"];

        //getting three random numbers according to the number of elements in the array
        var s1= Reel1[Math.floor(Math.random() * Reel1.length)]
        var s2= Reel2[Math.floor(Math.random() * Reel2.length)]
        var s3= Reel3[Math.floor(Math.random() * Reel3.length)]

        //displaying results to client
        document.getElementById("slot1").innerHTML = s1;
        document.getElementById("slot2").innerHTML = s2;
        document.getElementById("slot3").innerHTML = s3;

        //checking if the the three results are equal
        if(s1 === s2 && s1 === s3)
        {
            // swtich statment to increment score and showing message to client
            switch (s1) {
                case "cherry":
                    document.getElementById("score").innerHTML = score + 50;
                    document.getElementById("message").innerHTML = "Well Done 50 POINTS!";
                  return; 
                case "lemon":
                    document.getElementById("score").innerHTML = score + 3;
                    document.getElementById("message").innerHTML = "Spin Again only 3 POINTS!";
                    return;
                case "apple":
                    document.getElementById("score").innerHTML = score + 20;
                    document.getElementById("message").innerHTML = "GOOD 20 POINTS!";
                  return;
                case "banana":
                    document.getElementById("score").innerHTML = score + 15;
                    document.getElementById("message").innerHTML = "NOT BAD 15 POINTS!";
                    return;
                default:
                    document.getElementById("message").innerHTML ="BETTER LUCK NEXT TIME 0 POINTS";
                   return;
            }

        }
        //checking if at least two messages are the same
        else if (s1 === s2 || s1 === s3 || s3 === s2) {
            
            //checking which element came up twice
            var results = [s1,s2,s3];

            var modeMap = {};
            var maxEl = results[0], maxCount = 1;
            for(var i = 0; i < results.length; i++)
            {
                var el = results[i];
                if(modeMap[el] == null)
                    modeMap[el] = 1;
                else
                    modeMap[el]++;  
                if(modeMap[el] > maxCount)
                {
                    maxEl = el;
                    maxCount = modeMap[el];
                }
            }
            // swtich statment to increment score and showing message to client
            switch (maxEl) {
                case "cherry":
                    document.getElementById("score").innerHTML = score + 40;
                    document.getElementById("message").innerHTML = "Well Done 50 POINTS!";
                  return; 
                case "apple":
                    document.getElementById("score").innerHTML = score + 10;
                    document.getElementById("message").innerHTML = "GOOD 20 POINTS!"
                  return;
                case "banana":
                    document.getElementById("score").innerHTML = score + 5;
                    document.getElementById("message").innerHTML =  "NOT BAD 15 POINTS!";
                    return;
                default:
                    document.getElementById("message").innerHTML = "BETTER LUCK NEXT TIME 0 POINTS"; 
                   return;

        }
    }
        
    }

    render() {
        return (
           <div className="background-white DisplayData">
            <form onSubmit={this.initialize}>
                <h2>YoBetit Slot Machine</h2>
                <table>
                    <tbody>
                    <tr>
                        <td id="slot1"> ----</td>
                        <td id="slot2"> ----</td>
                        <td id="slot3"> ----</td>
                    </tr>
                    </tbody>
                </table>
                <button className="button-submit" type="submit">Spin Now</button>
                <p className="site-font"><span id="message"></span></p>   
                <p className="site-font">Score: <span id="score">0</span></p>  
                <p className="site-font">Credits: <span id="credits">20</span></p>              
            </form>
            </div>

           
        );
    }
}

export default MachineSlot;
