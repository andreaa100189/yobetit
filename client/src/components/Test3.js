
import React, { Component } from "react";

class SearchForm extends Component {
    constructor(props) {
        super(props);
        this.state = { country: "" };
    }

  

    changeHandler = (e) =>{
        this.setState({[e.target.name]: e.target.value})
    }
    //get all countries
    submitHandler = e => {
         fetch("http://localhost:9000/getallcountrieslist/")
        .then(res => res.json())
        .then(data => this.initialize(data))
        .catch(err => console.log("Error:", err));

    }

    //get countries according to the search
    submitHandler2 = e => {
        e.preventDefault()
         fetch("http://localhost:9000/getallcountries/"+ this.state['country'])
        .then(res => res.json())
        .then(data => this.initialize(data))
        .catch(err => console.log("Error:", err));

    }




    initialize (countriesData) {
        // Create a table.
        var col = ["Country", "Capital", "Flag"]
        var table = document.createElement("table");

        // Create table header row using the extracted headers above.
        var tr = table.insertRow(-1);                   // table row.

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // table header.
            th.innerHTML = col[i];
            tr.appendChild(th);
        }

        // add json data to the table as rows.
        for (var x = 0; x < countriesData.length; x++) {

            tr = table.insertRow(-1);

            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                if(j === 0){
                    tabCell.innerHTML = countriesData[x].name;
                }
                else if(j === 1){
                    tabCell.innerHTML = countriesData[x].capital;
                }
                else{
                    tabCell.innerHTML = '<img src="' + countriesData[x].flag + '" width="30px" height="30px" />';
                }
               
            }
        }

        // Now, add the newly created table with json data, to a container.
        var divShowData = document.getElementById('showData2');
        divShowData.innerHTML = "";
        divShowData.appendChild(table);
        }
       
    render() {
        const {country} = this.state
         return (         
                 
        <div className="background-white">
            {this.submitHandler()}
            <form onSubmit={this.submitHandler2}>
                <div>
                    <input className="search-input" placeholder="search here.." type="text" name="country" value={country} onChange={this.changeHandler}/>
                </div>
                <button className="button-submit" type="submit">Submit</button>               
            </form>
            <p className="DisplayData" id='showData2'>
                
            </p>
        </div>
        );
    }
}

export default SearchForm;



