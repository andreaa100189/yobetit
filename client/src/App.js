import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import CapitalForm from './components/CapitalForm'
import Test2 from "./components/Test2";
import Test3 from "./components/Test3";
import MachineSlot from "./components/Question4";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = { apiResponse: "" };
    }

    render() {
        return (
            <div className="App background-voilet">
                <h1>Get Country Details By Capital</h1>
               <CapitalForm />
               {/* <CapitalList />*/}

               <h1>Get Countries According To String</h1> 
               <Test2 />
               
               <h1>Search Country</h1>
               <Test3 />
                {/* <SearchList />*/}
                
                <h1>Machine Slot</h1>
                <MachineSlot />
               
            </div>
        );
    }
}

export default App;
