var express = require('express');
var router = express.Router();
const fetch = require('node-fetch');
var getJSON = require('get-json')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// Test 1 -- the capital is passed within the url and then passed as part from the
//           url to bring the desired data from the REST API 

router.get('/backend/:capital', function(req, res, next) {
  
  fetch("https://restcountries.eu/rest/v2/capital/" + req.params.capital)
  .then(response => response.json())
  .then(data => {
    res.send(data)
  })
});

// Test 2 list of string and compared with every country 

router.get('/test2/', function(req, res, next) {
 
  var myarray = [ 'aF', 'bu', 'Ma', 'cr', 'fR', 'au' ];
  var returneddata = [];
  
  getJSON('https://restcountries.eu/rest/v2/all', function(error, response){
  
    for(var i = 0; i < response.length;i++){
      for(var j = 0; j < myarray.length;j++){
        if(response[i].name.toLowerCase().includes(myarray[j].toLowerCase())){
           returneddata.push(response[i].name + ' contains ' + myarray[j].toLowerCase())
        }
      }
    }
    res.send(JSON.stringify(returneddata))
})
});

// Test 3 the search feature
router.get('/getallcountries/:country', function(req, res, next) {

  var returneddata = [];
 
    getJSON('https://restcountries.eu/rest/v2/all', function(error, response){  
    
      for(var i = 0; i < response.length;i++){
          if(response[i].name.toLowerCase().includes(req.params.country.toLowerCase())){
             returneddata.push(response[i])
          }
        }
      
      res.send(JSON.stringify(returneddata))

})
});

router.get('/getallcountrieslist/', function(req, res, next) {

 
    getJSON('https://restcountries.eu/rest/v2/all', function(error, response){  
      res.send(JSON.stringify(response))

})
});


module.exports = router;
