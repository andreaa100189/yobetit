DROP DATABASE Yobetit

CREATE DATABASE Yobetit

USE Yobetit
GO

CREATE TABLE Country(
	Country_Id  int NOT NULL  IDENTITY(1, 1),
    Country_Name varchar(255) NOT NULL,  
	Region_Name varchar(255) NOT NULL, 
	PRIMARY KEY (Country_Id)
);



CREATE TABLE Casino(
	Casino_Id int NOT NULL IDENTITY(1, 1),
    Casino_Name varchar(255) NOT NULL,
	Casino_Country_Id int NOT NULL,			
	foreign key (Casino_Country_Id) references Country(Country_Id),
    PRIMARY KEY (Casino_Id)
);

CREATE TABLE Game_Type(
	TypeId int NOT NULL IDENTITY(1, 1),
    TypeName varchar(255) NOT NULL,  
	PRIMARY KEY (TypeId)
);


CREATE TABLE Player(
	Player_Id int NOT NULL IDENTITY(1, 1),
    Player_Name varchar(255) NOT NULL,
	Player_Surname varchar(255) NOT NULL,
	Player_Age int NOT NULL,
	Player_DOB varchar(255) NOT NULL,
	Player_Email varchar(255) NOT NULL,
	Player_Phone varchar(255) NOT NULL,
	Player_Country_Id int NOT NULL,
	foreign key (Player_Country_Id) references Country(Country_Id),
    PRIMARY KEY (Player_Id)
);


CREATE TABLE Game(
	Game_Id int NOT NULL IDENTITY(1, 1),
    Game_Name varchar(255) NOT NULL,
	Game_Max_Players int NOT NULL,
	Game_Country_Id int NOT NULL,
	Game_Type_Id int NOT NULL,
	foreign key (Game_Type_Id) references Game_Type(TypeId),
    PRIMARY KEY (Game_Id)
);

CREATE TABLE Game_Country(
	Game_Country_Id int NOT NULL IDENTITY(1, 1),
    Game_Id int NOT NULL,
	Country_Id int NOT NULL,
	foreign key (Game_Id) references Game(Game_Id),
	foreign key (Country_Id) references Country(Country_Id),
    PRIMARY KEY (Game_Country_Id)
);

CREATE TABLE Favorite_Game(
	Favorite_Game_Id int NOT NULL IDENTITY(1, 1),
    Game_Id int NOT NULL,
	Player_Id int NOT NULL,
	foreign key (Game_Id) references Game(Game_Id),
	foreign key (Player_Id) references Player(Player_Id),
    PRIMARY KEY (Favorite_Game_Id)
);






INSERT INTO Country (Country_Name, Region_Name)
VALUES ('Malta', 'Europe'),
	   ('Italy', 'Europe'),
	   ('Dubai', 'Africa')


INSERT INTO Player(Player_Name,Player_Surname,Player_Age, Player_DOB, Player_Phone, Player_Email,Player_Country_Id)
VALUES ('Andre', 'Attard', 21, '14-09-1998', '+35679090998', 'andreattard12@gmail.com', 1),
	   ('Joe', 'Borg', 22, '15-09-1998', '+35679090997', 'andreattard@gmail.com', 2),
	   ('Maria', 'Caruana', 27, '19-09-1998', '+35679088997', 'marcar@gmail.com', 3)
	  
INSERT INTO Game_Type(TypeName)
VALUES ('SLOT'),
	   ('ROULETTE'),	
	   ('BETTING')

INSERT INTO Casino(Casino_Name,Casino_Country_Id)
VALUES ('Casino 1', 2),
	   ('Casino 2', 3),
	   ('Casino 3', 1)

INSERT INTO Game(Game_Name, Game_Max_Players, Game_Country_Id, Game_Type_Id)
VALUES ('SLOT MANIA', 1, 2, 1),
	   ('GAME 2', 1, 2, 2),
       ('GAME 3', 1, 1, 1)	

INSERT INTO Favorite_Game(Game_Id,Player_Id)
VALUES(1,1),
	  (2,2),
	  (1,3)

INSERT INTO Game_Country(Country_Id,Game_Id)
VALUES(1,1),
	  (2,2),
	  (1,3)


SELECT pl.Player_Name, pl.Player_Surname, pl.Player_Age, ga.Game_Name, gt.TypeName
FROM Player as pl JOIN Favorite_Game as fg
	ON pl.Player_Id = fg.Player_Id
	JOIN Game as ga
	ON fg.Game_Id = ga.Game_Id
	JOIN Game_Type as gt
	ON ga.Game_Type_Id = gt.TypeId
WHERE gt.TypeName = 'SLOT';

		
	   


